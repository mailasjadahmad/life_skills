### Q1. Behaviors Leading to Sexual Harassment:

#### Verbal Harassment:

- Comments about Clothing and Body: Inappropriate remarks about attire or appearance.
- Sexual or Gender-based Jokes or Remarks: Explicit jokes or gender-stereotyped comments.
- Requesting Sexual Favors: Unwanted advances or persistent date invitations.
- Sexual Innuendos: Subtle, suggestive remarks.
- Threats: Expressing harm related to sexual compliance.
- Spreading Rumors: False information about personal or sexual life.
- Using Obscene Language: Vulgar language with sexual undertones.

#### Visual Harassment:

- Explicit Visual Content: Inappropriate images or content.
- Sexual Emails or Texts: Messages with explicit content.

#### Physical Harassment:

- Sexual Assault: Unwanted physical contact or activity.
- Impeding Movement: Physically preventing free movement.
- Inappropriate Touching: Non-consensual physical contact.
- Sexual Gesturing or Staring: Suggestive body language or prolonged looks.

These unwelcome behaviors, when severe or pervasive, can create a hostile work environment, constituting sexual harassment.

### Q2. Responding to Harassment:

If I encounter or witness workplace harassment, I would:

1. **Report the Incident:**
   - Follow procedures, reporting to a supervisor, manager, or HR representative.

2. **Document Details:**
   - Keep a record of the incident, noting date, time, location, individuals, and description.

3. **Follow Employer Policies:**
   - Adhere to employer policies and guidelines.

4. **Seek Support:**
   - Discuss the incident with a trusted colleague, supervisor, or HR for guidance.

5. **Participate in Training:**
   - Actively engage in workplace training on harassment prevention.

6. **Stay Informed:**
   - Know employee rights, available resources, and procedures.

In summary, reporting harassment is a responsibility to foster a safe work environment. Seeking guidance from relevant authorities or employer resources is crucial if challenges persist.